## Overview

[[ TOC ]]

### `The Tech Stack`

`Hugo`, `Netfliy`, `Gitlab`, `Gitpod`. The theme used in Hugo based on an open source project. (which we in turn will also open source)

#### Diagrams
```mermaid
flowchart TD;
    hugo --> static[Create static site];
```

* [Hugo](https://gohugo.io/documentation/) - create static sites using templates
* [netlify](https://netlify.app) - allows for admin and creation of site content (create / comment / and commit to the git repo)
* [Gitlab](https://gitlab.com/adam.a.schulz/clcer-hugo-web) - hosts the source code and is connected with netlify to allow for commits from the admin panel
* [Gitpod](https://gitpod.io/workspaces) - allows for remote debugging and development


#### Configuration Files
`./config.toml`
`./netlify.toml`
`./static/admin/config.yml`



This codebase is hugo based static site generator, with netlify cms added. Most of these files will not need to be updated, however the `/content` and `/data` directories are the exception


### `/content`

Location for most of the copy. That files are in `/md` and will be used as the source of text when loaded by the corresponding page in `/layouts` more information can be found in the (https://gohugo.io/getting-started/directory-structure/)[Official Hugo Documentation]

### `/data`

Location for `/layout/partials` configurations. The files are in `.yaml` and are updated occationally.



/*============================================
 * Header
 *=============================================
*/

 function header(){
	 
	$('html').click(function(event) {
		if ($(event.target).closest('nav.mobile .container, #header .header').length === 0) {
			$('body').removeClass('show-nav');
			$('#header.option2 .toggle .navigation-toggle i').removeClass('fa-times').addClass('fa-bars');
		}
	});
	
	//down arrow for large billboard
	if($('#billboardDown').length){
		var hHeight = $('#header').outerHeight();
		var offsetDiff = 60;
		if($('#content').length){
			var firstSec = 'content';
		}else{
			var firstSec = $('.content-section').first().attr('id');
		}
		$('#billboardDown').click(function(event){
			event.preventDefault();
			$('html, body').stop().animate({scrollTop: $('#'+firstSec).offset().top - offsetDiff }, 550 );
		});
	}
	
	 //alert
	$('#alert-close').on('click', function(event) {
		event.preventDefault();
		Cookies.set('alertbar', 'off', { expires: 1, path: '/' });
		$('.alertbar-wrap.alertB3').slideUp();
	});
	
	/*Check header_option1 and header_option2 includes if you change this script + css*/

	if( $("body").hasClass("fixedheader") && $("#billboard").hasClass("hastext") ) {
		setBillboardHeight();
	}

	//desktop search
	$('.search-toggle').on('click', function(event) {
		event.preventDefault();
		$('body').removeClass('show-nav');
		$('#header.option2 .toggle .navigation-toggle i').removeClass('fa-times').addClass('fa-bars');
		
		$('.search-toggle i').toggleClass('fa-times fa-search');
		$('nav.desktop .search-container').fadeToggle();
		
		/*
		if( $('.alertbar-wrap.alertB3').length && $('.alertbar-wrap.alertB3').is(":hidden")) {
			$('.alertbar-wrap.alertB3').slideDown();
		}
		*/
	});

	//main nav
	$('.navigation-toggle').on('click', function(event) {
		event.preventDefault();
		$('body').toggleClass('show-nav');
		$('#header.option2 .toggle .navigation-toggle i').toggleClass('fa-bars fa-times');
		$('.search-toggle i').removeClass('fa-times').addClass('fa-search');
		$('nav.desktop .search-container').fadeOut();
		/*
		if( $('.alertbar-wrap.alertB3').length && isSiteSmall() ) {
			if( $("body").hasClass("show-nav") ){
				$('.alertbar-wrap.alertB3').slideUp();
			}
			else{
				$('.alertbar-wrap.alertB3').slideDown();
			}
		}
		*/
	});

	//mobile
	$('#mobilenav > li > ul > li a').each(function(){
		if($(this).parent().children("ul").length){
			$(this).parent().addClass('dropdown');
			var mnhref = $(this).attr('href');
			$(this).prepend('<span><i></i></span>');
			if(mnhref==''||mnhref=='#'){
				$(this).addClass('open');
			}else{
				$(this).find('span').addClass('open');
			}
		}
	});

	$('#mobilenav li.dropdown .open').on('click', function(event){
		var current = $(this).closest('li').first();

		current.siblings('.active').find('ul').first().slideUp();
		current.siblings('.active').removeClass('active');

		current.toggleClass('active');
		current.find('ul').first().slideToggle();
		event.preventDefault();
	});

	//predictive search
	//$searchInput = $('nav.desktop #search_term, nav.mobile #search_term2');
	$searchInput = $('nav.desktop #search_term');
	$searchInput.marcoPolo({
		url:"/_components/ajax/ajax-search-predictive.php",
		formatItem: function (data, $item) {
			  var title  = data.title,
				  type = data.type;
			  $item.attr('title', title);
			  return "<div class='title'>"+title+"</div><div class='type'>"+type+"</div>";
		},
		onRequestBefore: function(data) {
			$('.mp_list')
			  .removeClass('hasMore')
			  .show()
			  .html('<li class="searching"><em>Searching…</em></li>');
		},
		onResults: function(data) {
			if(data.length >= 7){
				$('.mp_list').addClass('hasMore');
				$('.mp_list li:last-child').addClass('mp_show_more');
			}
		},
		onSelect: function (data, $item) {
			var title = data.title;
			$searchInput.val(title);
			window.location = data.url;
		}
	});

	//countdown
    if($('#countdown').length>0){
	  window.$countdown = '#countdown';
	  $.getScript('/_assets/js/monk/helpers/countdown.js')
	  .done(function( script, textStatus ) {
		 if( $("body").hasClass("fixedheader") && $("#billboard").hasClass("hastext") ) {
		 	setBillboardHeight();
		 }
	  })
	  .fail(function( jqxhr, settings, exception ) {
		  //console.log('error');
	  });
  	}

	//header resets
	if(isSiteSmall() && $('.alertbar-wrap.alertB3').length){
		var navAdj = ($(window).height() - $('.alertbar-wrap.alertB3').outerHeight()) - 60;
		$('#header nav.mobile').css({'height' : navAdj + 'px'});
	}
	$(window).on('resize', $.debounce( 200, function() {
		if( $("body").hasClass("fixedheader") && $("#billboard").hasClass("hastext") ) {
			setBillboardHeight();
		}
		if(isSiteSmall()){
			if( $('.alertbar-wrap.alertB3').length ) {
				var navAdj = ($(window).height() - $('.alertbar-wrap.alertB3').outerHeight()) - 60;
				$('#header nav.mobile').css({'height' : navAdj + 'px'});
			}
		}else{
			$('#header nav.mobile').css({'height' : 'auto'});
		}
	}));
 }

 function setBillboardHeight() {
 	if($('#billboard.large').length && $('.alertbar-wrap.alertB3').length){
		var alertHeight = $('.alertbar-wrap.alertB3').outerHeight();
		$('#billboard.large').css({'min-height' : 'calc(100vh - '+alertHeight+'px)'});
	}
	
	if( $("#countdown-wrap").length ) {
		var countdownHeight = $("#countdown-wrap").outerHeight();
		if( $("#billboard .billboardDown").length ) {
			$('#billboard .billboardDown').css({'bottom': countdownHeight + 'px'});
			var downHeight = $('#billboard .billboardDown').outerHeight();
			countdownHeight += downHeight;
		}
		$('#billboard .inner').css({'padding-bottom': countdownHeight + 'px'});
	}
			
	
	var hHeight = $('#header').outerHeight() + 20;
	$('#billboard .inner').css({'padding-top': hHeight + 'px'});
	
 }

 /*============================================
 * Subnav
 *=============================================
*/
 function subnav(){

	$('#subnav li ul li a').each(function(){
		if($(this).parent().children("ul").length){
			$(this).parent().addClass('dropdown');
			var mnhref = $(this).attr('href');
			$(this).prepend('<span><i></i></span>');
			if(mnhref==''||mnhref=='#'){
				$(this).addClass('open');
			}else{
				$(this).find('span').addClass('open');
			}
		}
	});

	$('#subnav li.dropdown .open').on('click', function(event){
		var current = $(this).closest('li').first();

		current.siblings('.active').find('ul').first().slideUp();
		current.siblings('.active').removeClass('active');

		current.toggleClass('active');
		current.find('ul').first().slideToggle();
		event.preventDefault();
	});

	//for third level pages open up
	$('#subnav > li > ul > li.current > ul > li').parents('li.current').first().find('.open').trigger('click');

 }


/*============================================
 * Widgets
 *=============================================
*/
 function widgets(){

	//$(document).ajaxComplete(function() {
	//});

	//jqueryui
	$('.tabs').tabs({
		// OPTIONAL: allow for link to specific tab
		// beforeActivate: function (event, ui) {
		// 	history.pushState( null, null, "#"+ui.newPanel.attr('id') );
		// },
		// OPTIONAL: scroll to top of tab content when tab is clicked
		// activate: function( event, ui ) {
		// 	//$('html, body').stop().animate( { scrollTop: $('#'+ui.newPanel.attr('id')).offset().top - 32 }, 550 );
		// }
	});

	$('.tabs-linklist').each(function(){
		var tabsC = $(this).attr('id');
		var tabsID = tabsC.split('-')[1];
		$.ajax({
			url : '/_components/ajax/ajax-tabs.php',
			type: 'GET',
			data: ({id: tabsID}),
			success:function(results) {
				$('#'+tabsC).html(results);
				$('#'+tabsC).tabs({
					// OPTIONAL: allow for link to specific tab
					// beforeActivate: function (event, ui) {
					// 	history.pushState( null, null, "#"+ui.newPanel.attr('id') );
					// },
					// OPTIONAL: scroll to top of tab content when tab is clicked
					// activate: function( event, ui ) {
					// 	//$('html, body').stop().animate( { scrollTop: $('#'+ui.newPanel.attr('id')).offset().top - 32 }, 550 );
					// }
				});
			},
			error: function(results, textStatus, jqXHR) {
				//add error messages as needed console.log(jqXHR.responseText);
			}
		});
	});
	$('.accordion').accordion(
		{
        	header: "> h3",
			heightStyle: "content",
			active: false,
  			collapsible: true
      	}
	);

	//gallery monklet
	$('.insert-gallery').each(function(){

		var galleryID = $(this).attr('id');
		$('#'+galleryID).lightGallery({
			thumbnail:false,
			animateThumb: true,
			selector: '.item'
		});
	});

	if($('.packery').length){
		$.getScript('/_assets/js/lib/imagesloaded.min.js')
		  .done(function( script, textStatus ) {
			  $('.packery').each(function(){
				  var $grid = $(this).imagesLoaded( function() {
				  $grid.packery({
						columnWidth: '.item',
						gutter: 10,
						itemSelector: '.item',
						percentPosition: true
					});
				});

			});
		  })
		  .fail(function( jqxhr, settings, exception ) {
			  //console.log('error');
		  });
	}

	//media link icons
	$('.media-icons .video a').html("").append('<i class="fa fa-play"></i>');
	$('.media-icons .listen a').html("").append('<i class="fa fa-headphones"></i');
	$('.media-icons .download a').html("").append('<i class="fa fa-download"></i>');
	$('.media-icons .notes a').html("").append('<i class="fa fa-file-text-o"></i>');

	// Remove the Monk-generated onsubmit function and handle the form submit here
 	$('#newsletter').removeAttr('onsubmit').submit(function(event) {
 		// prevent default
 		event.preventDefault();
 		// validate input
 		var eml_addr = $(this).find('#newsletter_text').val();
 		if( isValidEmail(eml_addr) ) {
	 		// if valid, submit via ajax and present success message
	 		var formData = $(this).serialize();
	 		$.ajax({
	 			type: 'POST',
	 			url: $(this).attr('action'),
	 			data: formData
	 		});
			var successMsg = $(this).find('#success').val().split("||").pop();
	 		$(this).replaceWith("<h4 class='success-msg mb-0'>" + successMsg + "</h4>");
 		}
 		else { // Clear input and present alert
 			$(this).find('#newsletter_text').val = '';
 			$(this).find('#newsletter_text').attr('placeholder', 'email address');
 			alert('Please enter a valid email address and try again.');
 		}
 	});



	//campus/church map
	$('body').on('click', '#close_campus-map', function(event) {
		event.preventDefault();
		$('#select-church-map').remove();
		$('#select-church-widget').prop('selectedIndex',0);
		$('#select-church-widget').selectmenu('refresh');
	});

	$('#select-church-widget').change(function (event) {
		if (this.value == '') {
			if ($('#select-church-map').length) {
				$('#select-church-map').slideUp(300, function() {
					$('#select-church-map').remove();
				});
			}
		} else {
			if(!$('#select-church-map').length){
				if($(this).parents('.content-section').length){
				$(this).parents('.content-section').after('<div id="select-church-map"></div>');
				}else if($(this).parents('.content-main').length){
					$(this).parents('.widget.church-selector').after('<div id="select-church-map"></div>');
				}
			}
			loadChurchWidget($('#select-church-map'),this.value);
		}
	});

	// Church Widget: When only one church/campus is available, remove selector and just show the map
	if( $('.widget.church-selector').length ) {
		var $select = $('.widget.church-selector').find(".select > select.sorter");

		if( $select.children('option').length < 3 ) {
			// Get the slug/value of the single church
			var single_church_slug = $select.children('option').eq(1).val();
			// Different handling, depending on if the widget is in a content section or page area
			if( $select.parents('.content-section').length ){
				// Add the empty map-container element
				$select.parents('.content-section').after('<div id="select-church-map"></div>');
				// Load the map into its container
				loadChurchWidget($('#select-church-map'), single_church_slug );
				// Remove the original section
				$select.parents('.content-section').remove();
			}else if( $select.parents('.content-main').length ){
				// Add the empty map-container element
				$select.parents('.widget.church-selector').after('<div id="select-church-map"></div>');
				// Load the map into its container
				loadChurchWidget($('#select-church-map'), single_church_slug );
				// Remove the widget selector
				$select.parents('.widget.church-selector').remove();
			}
		}
	}

	function loadChurchWidget($cont,slug){
		$cont.addClass('active loading').html('');
		$.ajax({
			url : '/_components/ajax/ajax-church-map.php',
			type: "GET",
			data: ({'slug':slug}),
			success:function(results) {
				$cont.removeClass('loading').html(results);
			},
			error: function(data, textStatus, jqXHR) {
				//add error messages as needed console.log(jqXHR.responseText);
				//console.log(jqXHR.responseText);
			}
		});
	}

	//slick widget
	$('.slick-widget').each(function(){
		var howmany = parseInt($(this).data('howmany'));
		applySlick($(this), howmany);
		//team wants adj columns to match top padding when in row
		$(this).parents('[class^="col"]').siblings().css('padding-top','60px');
	});

	//build slick
	function applySlick($slickSlider, howmany){
		if(howmany){
			var slickShow = parseInt(howmany);
		}
		else{
			var slickShow = 3;
		}
		$slickSlider.slick({
			vertical: false,
			initialSlide: 0,
			slidesToShow: slickShow,
			slidesToScroll: 1,
			infinite: true,
			speed: 500,
			arrows: true,
			dots: false,
			responsive: [
			{
				//match css break
				breakpoint: 992,
					settings: {
						vertical: false,
						initialSlide: 0,
						slidesToShow: 1,
						slidesToScroll: 1,
						infinite: true
					}
				}
			]
		});
	}


 }

 /*============================================
 * isValidEmail()
 *=============================================
*/
function isValidEmail(emailAddress) {
  atPos=emailAddress.indexOf("@");
  dotPos = emailAddress.indexOf(".", atPos);
  spacePos = emailAddress.indexOf(" ");
  if ((emailAddress.length > 0) && (atPos > 0) && (dotPos > atPos) && (spacePos == -1)) {
    return true;
  }
  return false;
}

/*============================================
 * Responsive Tables
 *=============================================
*/
function tableInit(){
	function n(n){n.wrap("<div class='table-wrapper' />");var i=n.clone();i.find("td:not(:first-child), th:not(:first-child)").css("display","none"),i.removeClass("responsive-table"),n.closest(".table-wrapper").append(i),i.wrap("<div class='pinned' />"),n.wrap("<div class='scrollable' />"),e(n,i)}function i(n){n.closest(".table-wrapper").find(".pinned").remove(),n.unwrap(),n.unwrap()}function e(n,i){var e=n.find("tr"),t=i.find("tr"),a=[];e.each(function(n){var i=$(this),e=i.find("th, td");e.each(function(){var i=$(this).outerHeight(!0);a[n]=a[n]||0,i>a[n]&&(a[n]=i)})}),t.each(function(n){$(this).height(a[n])})}var t=!1,a=function(){return $(window).width()<767&&!t?(t=!0,$("table.responsive-table").each(function(i,e){n($(e))}),!0):void(t&&$(window).width()>767&&(t=!1,$("table.responsive-table").each(function(n,e){i($(e))})))};a(),$(window).on("redraw",function(){t=!1,a()}),$(window).on("resize",a)
}

/*============================================
 * Rotator
 *=============================================
*/
 function rotator(){

	if( $("#countdown-wrap").length ) {
		setTimeout(function() { 
			var countdownHeight = $("#countdown-wrap").outerHeight();
			if( $("#billboard .billboardDown").length ) {
				$('#billboard .billboardDown').css({'bottom': countdownHeight + 'px'});
				var downHeight = $('#billboard .billboardDown').outerHeight();
				countdownHeight += downHeight;
			}
			$('#billboard .inner').css({'padding-bottom': countdownHeight + 'px'});
		}, 1000);
	}
	//rotator
	$('.rotator .cycle-ss').each(function(){
		var $slideshow = $(this);
		var slidecount = $slideshow.find('.slide').length;
		if(slidecount<2){
			$('.rotator .cycle-pager').remove();
			$('.rotator .pager').remove();
		}
		$slideshow.on({
			'cycle-initialized': function(event, optionHash){
			},
			'cycle-post-initialize': function(event, optionHash){
			},
			'cycle-before': function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag){
				$('.rotator').removeClass('playing');
			},
			'cycle-after': function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag){
				//$slideshow.cycle('resume');
				if ($(outgoingSlideEl).hasClass('playing') ) {
					$(outgoingSlideEl).find('.video').remove();
					$(outgoingSlideEl).removeClass('playing');
					//$slideshow.cycle('resume');
				}
			}
		});
		$slideshow.cycle();// Auto initialization
	});

	// Video Slide
	$('.rotator .video-slide').click(function(event){
		event.preventDefault();
		var $this = $(this),
			mediaTitle = $this.data('title'),
			mediaType = $this.data('type'),
			mediaSRC = $this.data('src'),
			mediaImg = encodeURIComponent($this.data('image')),
			mediaExt = '';
			if(mediaType == 'file'){
				var mediaExt = mediaSRC.substr((mediaSRC.lastIndexOf('.')+1)).toLowerCase();
			}
			$this.parents('.cycle-ss').cycle('pause');
			$.ajax({
				url : '/_components/ajax/ajax-rotator-media.php',
				type: 'GET',
				data: ({title: mediaTitle, type: mediaType, src: mediaSRC, image: mediaImg, ext: mediaExt}),
				success:function(results) {
					$this.parents('.slide').addClass('playing').prepend(results);
				},
				error: function(results, textStatus, jqXHR) {
					//add error messages as needed console.log(jqXHR.responseText);
				}
			});
	});

 }
 
 /*
add new window to anchors
*/
function blankTarget(){
	$('[data-blanktarget=true]').each(function() {
		$(this).find('a').attr('target','_blank');
	});
	$(document).ajaxComplete(function() {
		$('[data-blanktarget=true]').each(function() {
			$(this).find('a').attr('target','_blank');
		});
	});
}

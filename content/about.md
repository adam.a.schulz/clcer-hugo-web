---
title: "WELCOME TO CENTRAL"
---

{{< youtube nA4LSaxuN2Y>}}

We are so glad you are here! Central is a Church of real people on the mission to Make Disciples Of Jesus. We revolve all we do around our values of Be, Grow, Serve, and Share.

## SUNDAY MORNING WORSHIP ##

8:30 AM - Liturgical
10:00 AM - Modern
